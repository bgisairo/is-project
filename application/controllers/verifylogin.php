<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class VerifyLogin extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('user','',TRUE);
	}
	function index()
	{
		//This method will have the credentials validation
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->form_validation->set_rules('username', 'Username',
			'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password',
			'trim|required|xss_clean|callback_check_database');
		if($this->form_validation->run() == FALSE)
		{
			//Field validation failed. User redirected to login page
			redirect('authenticator', 'refresh');
		}
		else
		{
			//$role_id=$this->input->post('role');
			$username = $this->input->post('username');
			$role_id = $this->Role_model->get_role_id($username);
			$rolename=$this->Role_model->get_role_name($role_id);
			//Go to private area //switch
			//big security hole here but basic user management seems to be up and running
			switch ($rolename) {
				case 'admin':
					redirect('admin', 'refresh');
					break;
				case 'doctor':
					redirect('doctor', 'refresh');
					break;
				
				default:
					redirect('authenticator', 'refresh');
					break;
			}
			
		}
	}
	function check_database($password)
	{
		//Field validation succeeded. Validate against database
		//will need to check for roles of each user from a dropdown in the login form
		$username = $this->input->post('username');
		//query the database
		$result = $this->user->login($username, $password);
		if($result)
		{
			$sess_array = array();
			foreach($result as $row)
			{
				$sess_array = array(
					'id' => $row->id,
					'username' => $row->Firstname
					);
				$this->session->set_userdata('logged_in', $sess_array);
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_database', 'Invalid username
				or password');
			return false;
		}
	}
}
?>
