<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authenticator extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}
	function index()
	{
		$data['roles'] = $this->Role_model->get_all_roles();
		$this->load->view('login_view', $data);
	}
	function logout()
	{
		if ($this->session->userdata) {
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			redirect('admin', 'refresh');
		}
		
	}

}

/* End of file authenticator.php */
/* Location: ./application/controllers/authenticator.php */
