<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lab extends CI_Controller {

    public function __construct()
    {
       
			parent::__construct();		
			//$this->load->library('grocery_CRUD');
		
		
    }
    function _example_output($output = null)
	{

		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$data['output'] = $output;
	 		$this->load->view('layouts/header.php', $output);
	 		$this->load->view('layouts/menubar.php', $data);
			$this->load->view('patient_view.php',$data['output']);
			$this->load->view('layouts/footer.php');	
		}else
		{	
//If no session, redirect to login page
			redirect('authenticator', 'refresh');
		}	
		
	}
	
    public function index()
    {
    	$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
    }

}

/* End of file patients.php */
/* Location: ./application/controllers/patients.php */