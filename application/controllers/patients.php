<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Patients extends CI_Controller {

    public function __construct()
    {
       
			parent::__construct();		
			//$this->load->library('grocery_CRUD');
		
		
    }
    function _example_output($output = null)
	{

		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$data['output'] = $output;
	 		$this->load->view('layouts/header.php', $output);
	 		$this->load->view('layouts/menubar.php', $data);
			$this->load->view('patient_view.php',$data['output']);
			$this->load->view('layouts/footer.php');	
		}else
		{	
//If no session, redirect to login page
			redirect('authenticator', 'refresh');
		}	
		
	}
	
    public function index()
    {
    	$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
    }

    public function all()
    {
    	try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('t_patient');
			$crud->set_subject('Patients');
			$crud->required_fields('c_fname','c_lname', 'c_email', 'c_password');
			// $crud->columns('city','country','phone','addressLine1','postalCode');
			
			$output = $crud->render();
			$this->_example_output($output);		
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	
    }

}

/* End of file patients.php */
/* Location: ./application/controllers/patients.php */