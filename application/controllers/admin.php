<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Admin extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{	
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			//$this->load->view('home_view', $data);
			$output = (object)array('output' => '' , 'js_files' => array() , 'css_files' => array());
			//end loading dummy output

			$this->load->view('layouts/header.php', $output);
	 		$this->load->view('layouts/menubar.php', $data);
			$this->load->view('admin/home_view.php',$data);
			$this->load->view('layouts/alternate_footer.php');
		}
		else
		{
//If no session, redirect to login page
			redirect('authenticator', 'refresh');
		}
	}

	function _example_output($output = null)
	{

		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$data['output'] = $output;
	 		$this->load->view('layouts/header.php', $output);
	 		$this->load->view('layouts/menubar.php', $data);
			$this->load->view('admin/output_view.php',$data['output']);
			$this->load->view('layouts/footer.php');	
		}else
		{	
//If no session, redirect to login page
			redirect('authenticator', 'refresh');
		}	
		
	}

    public function patients()
    {
    	try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('t_patient');
			$crud->set_subject('Patient');
			$crud->add_action('Billing');
			$crud->required_fields('Firstname','Lastname', 'Age', 'Sex', 'Cellphone', 'Address');
			$crud->set_rules('Firstname', 'Firstname', 'required|alpha');
			$crud->set_rules('Lastname', 'Lastname', 'required|alpha');
			$crud->set_rules('Age', 'Age', 'required|numeric|is_natural');
			$crud->set_rules('Cellphone', 'Cellphone', 'required|numeric');
			$crud->set_rules('Address', 'Address', 'required|numeric|alpha_numeric');
			$crud->set_relation('Sex', 't_sex', 'name');
			// $crud->columns('city','country','phone','addressLine1','postalCode');
			
			$output = $crud->render();
			$this->_example_output($output);		
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	
    }

    public function employees()
    {
    	try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('t_personel');
			$crud->set_subject('Employees');
			$crud->columns('Firstname','Lastname', 'Email', 'Role_id');
			$crud->required_fields('Firstname','Lastname', 'Role_id', 'Email', 'Password');
			$crud->set_relation('Role_id','t_roles','name');
			$crud->set_relation('department_id', 't_departments', 'name');
			$crud->set_rules('Firstname', 'Firstname', 'required|alpha');
			$crud->set_rules('Lastname', 'Lastname', 'required|alpha');
			$crud->set_rules('Email', 'Email', 'required|valid_email');
			$crud->unset_export();
			$crud->callback_before_insert(array($this, 'encrypt_password_callback'));
			// $crud->columns('city','country','phone','addressLine1','postalCode');
			
			$output = $crud->render();
			$this->_example_output($output);		
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }

    public function encrypt_password_callback($post_array, $primary_key)
    {
    	if (!empty($post_array['Password'])) {
    		$post_array['Password'] = $this->encrypt->sha1($post_array['Password']);
    	}else{
    		unset($post_array['Password']);
    	}
    	return $post_array;
    }
    public function department()
    {
    	try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('t_departments');
			$crud->set_subject('Departments');
			$crud->columns('name');
			$crud->required_fields('name');
			$crud->unset_export();
			// $crud->callback_before_insert(array($this, 'encrypt_password_callback'));
			// $crud->columns('city','country','phone','addressLine1','postalCode');
			
			$output = $crud->render();
			$this->_example_output($output);		
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }

    public function requisition()
    {
    	try{
			/* This is only for the autocompletion */
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('t_requisitions');
			$crud->set_subject('Requisition');
			$crud->columns('Name','Description', 'Quantity', 'Price', 'Expiry_date', 'Date_recieved', 'vendor_id');
			$crud->required_fields('Name','Description', 'Quantity', 'Price', 'Expiry_date', 'Date_recieved', 'vendor_id');
			$crud->set_relation('vendor_id','t_vendors','name');
			$crud->set_rules('Name', 'Name', 'required|alpha');
			// $crud->set_rules('Description', 'Description', 'required|alpha');
			$crud->set_rules('Price', 'Price', 'required|decimal');
			$crud->set_rules('Quantity', 'Quantity', 'required|decimal');
			$crud->unset_export();
			// $crud->callback_before_insert(array($this, 'encrypt_password_callback'));
			// $crud->columns('city','country','phone','addressLine1','postalCode');
			
			$output = $crud->render();
			$this->_example_output($output);		
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
}
?>
