<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role_model extends CI_Model {

	public function get_role_id($username)
	{
		$q = $this->db->get_where('t_personel', array('Email'=>$username));
		if ($q->num_rows() > 0) {
			$row = $q->row();
			return $row->Role_id;
		}
	}

    function get_all_roles()
	{
		$q = $this->db->get('t_roles');
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
	}

	public function get_role_name($id)
	{
		$query = $this->db->get_where('t_roles', array('id'=> $id));
		if ($query->num_rows() > 0)
		{
		   $row = $query->row();
		   return $row->name;
		}
	}
	function login($username, $password)
	{
		$this -> db -> select('id, Email, Password, Firstname');
		$this -> db -> from('t_personel');
		$this -> db -> where('Email = ' . "'" . $username . "'");
		$this -> db -> where('Password = ' . "'" . MD5($password) . "'");
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		
		if($query -> num_rows() == 1){
			return $query->result();
		}
		else{
			return false;
		}
	}

}

/* End of file role.php */
/* Location: ./application/models/role.php */