<article class="row">
    <div class="span12">
        <div class="span5">
            <table class="table table-hover">
            <caption>#1</caption>
            <thead>
                <tr>
                    <th> Options</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/admin/patients">Patients</a> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/admin/employees">Employees</a> 
                    </td>
                </tr>
                 <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/admin/department">Departments</a> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/admin/requisition">Requisition</a> 
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
        <div class="span5">
            <table class="table table-hover">
            <caption>#2</caption>
            <thead>
                <tr>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/admin/billing">Patient Payments</a> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/admin/patient">View Patient</a> 
                    </td>
                </tr>
            </tbody>
        </table>
        </div>     
    </div>
</article>

