        <header>
            <div class="navbar navbar-inverse navbar-static-top">
                <div class="navbar-inner">
                    <a class="brand" href="#">Hillside Hospital</a>
                    <ul class="nav">
                        <li class="active">
                            <a href="<?php echo site_url() //this path has to be dynamic?>/admin" >Home</a>
                        </li>
                        <li class="dropdown" >
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
                            <?php echo $username; ?>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                    <li><a href="<?php echo site_url() ?>/authenticator/logout">Logout</a></li>
                            </ul>
                        </li>      
                    </ul>
                </div>
            </div>
            <div class="span11">
                <div class="span2"></div>
                <br>
                <div class="span9">
                     <?php echo set_breadcrumb(); ?>
                </div>
            </div>
        </header>