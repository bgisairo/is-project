<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <!-- Bootstrap -->
    <!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet"></head>
    -->
    <?php echo link_tag("assets/css/bootstrap.min.css");?>
   <link href="<?php echo base_url() ?>/assets/css/style.css" rel="stylesheet">
<body>
    <div class="container">
        <!-- <form class="form-horizontal" >
        -->
        <header id="loginheader">
            <div class="navbar navbar-inverse">
                <div class="navbar-inner">
                    <a class="brand" href="#">Hillside Hospital</a>
                    <ul class="nav">
                        <li class="active">
                            <a href="#">Home</a>
                        </li>
            
                    </ul>
                </div>
            </div>
        </header>
        <article id="loginarticle" class="row">
            <div class="loginformholder span12">
                <?php echo validation_errors(); ?>
                <?php $attributes = array('class' =>
                'form-horizontal', 'id' => 'myform'); ?>
                <?php echo form_open('verifylogin', $attributes); ?>
                <div class="control-group">
                    <label class="control-label" for="username">Username</label>
                    <div class="controls">
                        <input type="text" id="username" placeholder="Email" name="username"></div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="password">Password</label>
                    <div class="controls">
                        <input type="password" id="password" name="password" placeholder="Password"></div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn">Sign in</button>
                    </div>
                </div>
                </form>
            </div>
        </article>
        <footer id="loginfooter">
            <div class="navbar navbar-inverse ">
                <div class="navbar-inner">
                    <ul class="nav">
                        <li>
                            <a href="#">&copy; Hillside Hospital 2012</a>
                        </li>
            
                    </ul>
                </div>
            </div>
        </footer>
        
</div>

<script src="<?php echo base_url(); ?>/assets/js/jquery-1.8.1.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
</body>
</html>