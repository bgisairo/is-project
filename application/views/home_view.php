<article class="row">
    <div class="span12">
        <h3>Welcome: Choose a task:</h3>
        <div class="span5">
            <table class="table table-hover">
            <caption>Patient Help</caption>
            <thead>
                <tr>
                    <th> Options</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/patients/all">Patients</a> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/beds/">Bed Allotment</a> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/admission/">Admissions</a> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/discharge/">Discharges</a> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/visit/">Patient Visit</a> 
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
        <div class="span5">
            <table class="table table-hover">
            <caption>Staff  Help</caption>
            <thead>
                <tr>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/patients/all">Patient Payments</a> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="<?php echo site_url() ?>/patients/all">View Patient</a> 
                    </td>
                </tr>
            </tbody>
        </table>
        </div>     
    </div>
</article>

